import unittest

from src.enums.error_code_enum import ErrorCodeEnum
from src.enums.gender_enum import GenderEnum
from src.services.address_parser_service import AddressParserService


class ParserTests(unittest.TestCase):
    def test_empty_input(self):
        # Arrange
        parser_input = ""

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "")
        self.assertEqual(address.sur_name, "")
        self.assertEqual(error_list, [])

    def test_only_names(self):
        # Arrange
        parser_input = "Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_only_one_name(self):
        # Arrange
        parser_input = "Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_all_given(self):
        # Arrange
        parser_input = "Herr Dr. Prof. Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_without_title(self):
        # Arrange
        parser_input = "Herr Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_without_address(self):
        # Arrange
        parser_input = "Dr. Prof. Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_without_name(self):
        # Arrange
        parser_input = "Herr Dr. Prof. Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_one_title(self):
        # Arrange
        parser_input = "Herr Dr. Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_several_titles(self):
        # Arrange
        parser_input = "Herr Dr. Prof. Dr.-Ing. Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof.", "Dr.-Ing."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_multiple_names(self):
        # Arrange
        parser_input = "Peter Günther Hans Hermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertTrue(len(error_list) == 0)
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Peter")
        self.assertEqual(address.sur_name, "Günther Hans Hermann")

    def test_false_order(self):
        # Arrange
        parser_input = "Max Dr. Prof. Herr Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertTrue(error_list.__contains__(ErrorCodeEnum.false_title_address_position))
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")

    def test_title_in_name(self):
        # Arrange
        parser_input = "Peter Herrmann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Peter")
        self.assertEqual(address.sur_name, "Herrmann")
        self.assertEqual(error_list, [])

    def test_false_title(self):
        # Arrange
        parser_input = "Herr Dr. ABC Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "ABC")
        self.assertEqual(address.sur_name, "Max Mustermann")
        self.assertEqual(error_list, [])

    def test_false_address(self):
        # Arrange
        parser_input = "ABC Dr. Prof. Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "ABC")
        self.assertEqual(address.sur_name, "Max Mustermann")
        self.assertEqual(error_list, [])

    def test_false_title_and_order(self):
        # Arrange
        parser_input = "Herr Max ABC Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "ABC Mustermann")
        self.assertEqual(error_list, [])

    def test_title_of_nobility(self):
        # Arrange
        parser_input = "Herr Dr. Prof. Max Imperator von Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Imperator von Mustermann")
        self.assertEqual(error_list, [])

    def test_prefix_sur_name(self):
        # Arrange
        parser_input = "Herr Dr. Prof. Max van Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "van Mustermann")
        self.assertEqual(error_list, [])

    def test_number_input(self):
        # Arrange
        parser_input = "Herr Dr. Prof. Max1 123 Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertTrue(error_list.__contains__(ErrorCodeEnum.number))
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")

    def test_special_character_input(self):
        # Arrange
        parser_input = "Herr! Dr. Prof. $Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertTrue(error_list.__contains__(ErrorCodeEnum.special_char))
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")

    def test_title_duplicates(self):
        # Arrange
        parser_input = "Herr Prof. Prof. Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_title_length_problem(self):
        # Arrange
        parser_input = "Herr Prof. Dr.-Ing. Max Mustermann"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Prof.", "Dr.-Ing."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])


if __name__ == '__main__':
    unittest.main()
