import unittest

from src.services.address_parser_service import AddressParserService


class AddAddressTitleTests(unittest.TestCase):
    def test_add_none(self):
        # Arrange
        title_input = None
        amount_titles = len(AddressParserService.address_titles)

        # Test
        # noinspection PyTypeChecker
        result = AddressParserService.add_address_title(title_input)

        # Assert
        self.assertFalse(result)
        self.assertEqual(len(AddressParserService.address_titles), amount_titles)

    def test_add_empty(self):
        # Arrange
        title_input = ''
        amount_titles = len(AddressParserService.address_titles)

        # Test
        result = AddressParserService.add_address_title(title_input)

        # Assert
        self.assertFalse(result)
        self.assertEqual(len(AddressParserService.address_titles), amount_titles)

    def test_add_normal_title(self):
        # Arrange
        title_input = 'testTitel'
        amount_titles = len(AddressParserService.address_titles)

        # Test
        result = AddressParserService.add_address_title(title_input)

        # Assert
        self.assertTrue(result)
        self.assertEqual(len(AddressParserService.address_titles) - 1, amount_titles)
        self.assertTrue(AddressParserService.address_titles.__contains__('testTitel'))

    def test_add_composite_title(self):
        # Arrange
        title_input = 'test-Titel.'
        amount_titles = len(AddressParserService.address_titles)

        # Test
        result = AddressParserService.add_address_title(title_input)

        # Assert
        self.assertTrue(result)
        self.assertEqual(len(AddressParserService.address_titles) - 1, amount_titles)
        self.assertTrue(AddressParserService.address_titles.__contains__('test-Titel.'))

    def test_add_title_of_multiple_words(self):
        # Arrange
        title_input = 'test Titel.'
        amount_titles = len(AddressParserService.address_titles)

        # Test
        result = AddressParserService.add_address_title(title_input)

        # Assert
        self.assertTrue(result)
        self.assertEqual(len(AddressParserService.address_titles) - 1, amount_titles)
        self.assertTrue(AddressParserService.address_titles.__contains__('test Titel.'))


if __name__ == '__main__':
    unittest.main()

