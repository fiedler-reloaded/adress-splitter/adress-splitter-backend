import unittest

from src.enums.error_code_enum import ErrorCodeEnum
from src.enums.gender_enum import GenderEnum
from src.services.address_parser_service import AddressParserService


class ParseCommaTests(unittest.TestCase):
    def test_without_titles(self):
        # Arrange
        parser_input = "Mustermann, Max-Felix"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, [])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_titles(self):
        # Arrange
        parser_input = "Mustermann, Max-Felix, Dr. Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_titles_prefix_din(self):
        # Arrange
        parser_input = "Mustermann, Max-Felix von, Dr. Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "von Mustermann")
        self.assertEqual(error_list, [])

    def test_titles_prefix_title_of_nobility_din(self):
        # Arrange
        parser_input = "Mustermann, Max-Felix Freiherr von, Dr. Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Freiherr von Mustermann")
        self.assertEqual(error_list, [])

    def test_non_din_prefix_position(self):
        # Arrange
        parser_input = "von Mustermann, Max-Felix, Dr. Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "von Mustermann")
        self.assertEqual(error_list, [])

    def test_non_din_title_of_nobility_position(self):
        # Arrange
        parser_input = "Freiherr von Mustermann, Max-Felix, Dr. Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Freiherr von Mustermann")
        self.assertEqual(error_list, [])

    def test_false_and_true_title_position(self):
        # Arrange
        parser_input = "Dr. Mustermann, Max-Felix, Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertTrue(error_list.__contains__(ErrorCodeEnum.false_title_address_position))
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Mustermann")

    def test_false_title_position(self):
        # Arrange
        parser_input = "Dr. Prof. Mustermann, Max-Felix"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertTrue(error_list.__contains__(ErrorCodeEnum.false_title_address_position))
        self.assertEqual(address.address, "")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.not_specified)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Mustermann")

    def test_address_in_sur_name(self):
        # Arrange
        parser_input = "Herr Mustermann, Max-Felix, Dr. Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_address_in_name(self):
        # Arrange
        parser_input = "Mustermann, Herr Max-Felix, Dr. Prof."

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertEqual(address.address, "Herr")
        self.assertEqual(address.title, ["Dr.", "Prof."])
        self.assertEqual(address.gender, GenderEnum.male)
        self.assertEqual(address.name, "Max-Felix")
        self.assertEqual(address.sur_name, "Mustermann")
        self.assertEqual(error_list, [])

    def test_more_than_two_commas_error(self):
        # Arrange
        parser_input = "Mustermann, Max-Felix, Dr. Prof., Test"

        # Test
        address, error_list = AddressParserService.parse(parser_input)

        # Assert
        self.assertTrue(error_list.__contains__(ErrorCodeEnum.false_input))
        self.assertEqual(address, None)


if __name__ == '__main__':
    unittest.main()
