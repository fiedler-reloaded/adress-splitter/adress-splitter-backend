import unittest

from src.services.address_parser_service import AddressParserService


class DeleteAddressTitleTests(unittest.TestCase):
    def test_delete_none(self):
        # Arrange
        title_input = None
        amount_titles = len(AddressParserService.address_titles)

        # Test
        # noinspection PyTypeChecker
        result = AddressParserService.delete_address_title(title_input)

        # Assert
        self.assertFalse(result)
        self.assertEqual(len(AddressParserService.address_titles), amount_titles)

    def test_delete_empty(self):
        # Arrange
        title_input = ''
        amount_titles = len(AddressParserService.address_titles)

        # Test
        result = AddressParserService.delete_address_title(title_input)

        # Assert
        self.assertFalse(result)
        self.assertEqual(len(AddressParserService.address_titles), amount_titles)

    def test_add_normal_title(self):
        # Arrange
        title_input = 'testTitel'
        add_result = AddressParserService.add_address_title(title_input)
        amount_titles = len(AddressParserService.address_titles)

        # Test
        result = AddressParserService.delete_address_title(title_input)

        # Assert
        self.assertTrue(add_result)
        self.assertTrue(result)
        self.assertEqual(len(AddressParserService.address_titles), amount_titles - 1)
        self.assertFalse(AddressParserService.address_titles.__contains__('testTitel'))

    def test_delete_false_title(self):
        # Arrange
        title_input = 'ABC.'
        amount_titles = len(AddressParserService.address_titles)

        # Test
        result = AddressParserService.delete_address_title(title_input)

        # Assert
        self.assertFalse(result)
        self.assertEqual(len(AddressParserService.address_titles), amount_titles)


if __name__ == '__main__':
    unittest.main()
