import unittest

from src.enums.gender_enum import GenderEnum
from src.enums.language_enum import LanguageEnum
from src.objects.address import Address


class MyTestCase(unittest.TestCase):
    def test_letter_adress_german_male(self):
        # Arrange
        test_adress = Address(["Dr."], "Mustermann", "Max", GenderEnum.male, "Herr")

        # Test
        letter_adress = test_adress.get_letter_address(LanguageEnum.german)

        # Assert
        self.assertEqual("Sehr geehrter Herr Dr. Max Mustermann", letter_adress)

    def test_letter_adress_german_female(self):
        # Arrange
        test_adress = Address(["Dr."], "Mustermann", "Max", GenderEnum.female, "Frau")

        # Test
        letter_adress = test_adress.get_letter_address(LanguageEnum.german)

        # Assert
        self.assertEqual("Sehr geehrte Frau Dr. Max Mustermann", letter_adress)

    def test_letter_adress_german_nonbinary(self):
        # Arrange
        test_adress = Address([], "Mustermann", "Max", GenderEnum.non_binary, "")

        # Test
        letter_adress = test_adress.get_letter_address(LanguageEnum.german)

        # Assert
        self.assertEqual("Guten Tag Max Mustermann", letter_adress)

    def test_letter_adress_german_not_specified(self):
        # Arrange
        test_adress = Address([], "Mustermann", "", GenderEnum.not_specified, "")

        # Test
        letter_adress = test_adress.get_letter_address(LanguageEnum.german)

        # Assert
        self.assertEqual("Sehr geehrte Damen und Herren", letter_adress)

    def test_letter_adress_english(self):
        # Arrange
        test_adress = Address(["doctor"], "Musterman", "Max", GenderEnum.not_specified, "Mr.")

        # Test
        letter_adress = test_adress.get_letter_address(LanguageEnum.english)

        # Assert
        self.assertEqual("Dear Mr. doctor Max Musterman", letter_adress)


if __name__ == '__main__':
    unittest.main()
