import re

from src.enums.error_code_enum import ErrorCodeEnum
from src.enums.gender_enum import GenderEnum
from src.enums.language_enum import LanguageEnum
from src.objects.address import Address


class AddressParserService:
    address_male = ['Herr', 'Mr.', 'Mr']
    address_female = ['Frau', 'Ms.', 'Ms', 'Mrs.', 'Mrs']
    address_titles = ['Professor', 'Prof.', 'professor', 'prof.', 'doctor', 'doct.', 'doc.', 'Doktor',
                      'Dipl. Kunstpädagogik', 'Dipl. Ing.', 'Dr.-Ing.', 'Dr.']
    special_chars = ['@', '!', '$', '%', '^', '&', '*', '(', ')', '<', '>', '?', '\\', '|', '{', '}', '~', ':', '[',
                     ']',
                     '#']

    @staticmethod
    def build_regex(elem_list: list[str]) -> r"":
        """
        Helper method to build regex from list of strings

        :param elem_list: list[str] - list to build regex
        :return: r"" - resulted regex of input element_list
        """
        constructed_regex = r""
        for index, element in enumerate(elem_list):
            constructed_regex += element.replace(".", "\\.")
            if index < len(elem_list) - 1:
                constructed_regex += "|"
        return constructed_regex

    @staticmethod
    def __check_digit_or_special_chars(input_address: str) -> (str, set):
        """
        Helper method to check input string for digits and special chars

        :param input_address: str - input string to check for
        :return: str - checked input_address string
        """
        error_list = set()
        temp_input = ""
        for char in input_address:
            if char.isdigit():
                error_list.add(ErrorCodeEnum.number)
            elif AddressParserService.special_chars.__contains__(char) is True:
                error_list.add(ErrorCodeEnum.special_char)
            else:
                temp_input += char
        return temp_input, error_list

    @staticmethod
    def __get_address_and_gender(input_address: str) -> (str, GenderEnum):
        """
        Helper method to get address and gender of input string

        :param input_address: str - input string to generate address and gender
        :return: (str, GenderEnum) - address and gender as tuple
        """
        address_list = AddressParserService.build_regex(
            AddressParserService.address_male + AddressParserService.address_female)
        address = ""
        split_input = input_address.split()
        for split in split_input:
            if address_list.__contains__(split):
                address = split
                break
        if address in AddressParserService.address_male:
            gender = GenderEnum.male
        elif address in AddressParserService.address_female:
            gender = GenderEnum.female
        else:
            gender = GenderEnum.not_specified

        return address, gender

    @staticmethod
    def __reduce_address_and_titles(input_address: str, address: str, titles: list[str], error_list: set) -> (str, set):
        """
        Helper method to delete address and titles of input string and update error_list

        :param input_address: str - input string to parse
        :param address: str - address to delete from input_address
        :param titles: list[str] - titles to delete from input_address
        :param error_list: set - error_list of input_address
        :return: (Address, list[ErrorCodeEnum]) - updated tuple with address-object and error-code-list
        """
        input_address = input_address.replace(address, "")
        for title in titles:
            input_address = input_address.replace(title, "", 1)

        return input_address, error_list

    @staticmethod
    def __parse_comma(checked_input_address: str, error_list: set) -> (Address, set):
        """
        Private method to parse address input string with one or two commas

        :param checked_input_address: str - checked input string to parse
        :param error_list: set - error_list of checked_input_address
        :return: (Address, list[ErrorCodeEnum]) - tuple with address-object and error-code-list
        """
        titles = re.findall(AddressParserService.build_regex(AddressParserService.address_titles), checked_input_address)

        if checked_input_address.count(',') == 2:
            joined_titles = " ".join(titles)
            input_titles = re.split(",", checked_input_address)[2].strip()
            if input_titles != joined_titles:
                error_list.add(ErrorCodeEnum.false_title_address_position)
        elif len(titles) > 0:
            error_list.add(ErrorCodeEnum.false_title_address_position)

        address, gender = AddressParserService.__get_address_and_gender(checked_input_address)

        reduced_input_address, reduced_error_list = AddressParserService.__reduce_address_and_titles(
            input_address=checked_input_address, address=address, titles=titles, error_list=error_list)
        error_list = error_list.union(reduced_error_list)

        reduced_split = re.split(",", reduced_input_address)
        sur_name = reduced_split[0]
        name = reduced_split[1]
        split_name = name.split()
        if len(split_name) > 1:
            joined_split_name = " ".join(split_name[1:])
            name = name.replace(joined_split_name, "")
            sur_name = f"{joined_split_name} {sur_name}"

        address_object = Address(list(dict.fromkeys(titles)), sur_name.strip(), name.strip(), gender, address)
        return address_object, error_list

    @staticmethod
    def parse(input_address: str) -> (Address, list[ErrorCodeEnum]):
        """
        Parsing input string to Address-object. Collect Errors in returned list

        :param input_address: str - input string to parse
        :return: (Address, list[ErrorCodeEnum]) - tuple with address-object and error-code-list
        """
        checked_input_address, error_list = AddressParserService.__check_digit_or_special_chars(input_address)

        comma_count = checked_input_address.count(',')
        if comma_count > 2:
            error_list.add(ErrorCodeEnum.false_input)
            return None, list(error_list)
        elif comma_count > 0:
            address, error_list = AddressParserService.__parse_comma(checked_input_address, error_list)
            return address, list(error_list)

        address, gender = AddressParserService.__get_address_and_gender(checked_input_address)
        titles = re.findall(AddressParserService.build_regex(AddressParserService.address_titles), checked_input_address)

        joined_titles = " ".join(titles)
        if input_address.startswith(f"{address} {joined_titles}") is False and len(titles) > 0 and address != "":
            error_list.add(ErrorCodeEnum.false_title_address_position)

        reduced_input_address, error_list = AddressParserService.__reduce_address_and_titles(
            input_address=checked_input_address, address=address, titles=titles, error_list=error_list)

        input_split = reduced_input_address.split()
        if len(input_split) == 0:
            sur_name = ""
            name = ""
        elif len(input_split) == 1:
            sur_name = reduced_input_address
            name = ""
        else:
            name = reduced_input_address.split()[0]
            sur_name = reduced_input_address.replace(name, "")

        address_object = Address(list(dict.fromkeys(titles)), sur_name.strip(), name.strip(), gender, address)
        return address_object, list(error_list)

    @staticmethod
    def get_letter_address(address: Address, language: LanguageEnum) -> str:
        """
        Generate letter address of address object with specific language

        :param address: Address - address object to generate letter address
        :param language: LanguageEnum - language type of address input
        :return: str - formatted letter address
        """
        return address.get_letter_address(language)

    @staticmethod
    def add_address_title(title: str) -> bool:
        """
        Adding address title to address_titles of AddressParserService

        :param title: str - title to add
        :return: bool - if method is successfully return true, else false
        """
        if title is None or title == '':
            return False
        elif next((x for x in AddressParserService.address_titles if x == title), None) is None:
            AddressParserService.address_titles.append(title)
            AddressParserService.address_titles.sort(key=len, reverse=True)
            return True
        elif next((x for x in AddressParserService.address_titles if x == title), None) is not None:
            return True
        else:
            return False

    @staticmethod
    def delete_address_title(title: str) -> bool:
        """
        Deleting address title in address_titles of AddressParserService

        :param title: str - title to add
        :return: bool - if method is successfully return true, else false
        """
        title_to_delete = next((x for x in AddressParserService.address_titles if x == title), None)
        if title_to_delete is None:
            return False
        else:
            AddressParserService.address_titles.remove(title_to_delete)
            return True

    @staticmethod
    def get_address_titles() -> list[str]:
        """
        Get all current address titles of AddressParserService

        :return: list[str] - list containing all titles, which are recognized
        """
        return AddressParserService.address_titles
