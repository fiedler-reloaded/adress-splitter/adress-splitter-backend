from enum import Enum


class LanguageEnum(Enum):
    german = 0
    english = 1
    automatic = 2
