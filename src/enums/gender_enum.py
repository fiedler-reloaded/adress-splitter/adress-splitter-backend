from enum import Enum


class GenderEnum(Enum):
    male = "male"
    female = "female"
    non_binary = "non binary"
    not_specified = "not specified"
