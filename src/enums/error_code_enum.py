from enum import Enum


class ErrorCodeEnum(Enum):
    false_title_address_position = 187
    special_char = 69
    number = 66
    false_input = 420

