from flask import current_app as app, request, jsonify

from src.enums.language_enum import LanguageEnum
from src.objects.address import Address
from src.services.address_parser_service import AddressParserService


@app.route('/parse/<address>', methods=['POST'])
def parse(address: str):
    """
    Interface of parsing address with AddressParserService for frontend

    :param address: str - not formatted address
    :return: Address - return parsed address as Address object
    """
    address, error_list = AddressParserService.parse(address)
    error_list = [code.value for code in error_list]
    return jsonify({"address": None if address is None else address.serialize(), "error_list": error_list})


@app.route('/getLetterAddress/<language>', methods=['POST'])
def get_letter_address(language: str):
    """
    Interface of getting letter address of an address-object with AddressParserService for frontend

    :param language: LanguageEnum - language type of address input
    :return: str - formatted letter address
    """
    address = Address.deserialize(request.get_json(force=True)['address'])
    return jsonify(AddressParserService.get_letter_address(address, LanguageEnum(int(language))))


@app.route('/address-title/<title>', methods=['POST'])
def add_address_title(title: str):
    """
    Interface of adding an address to address_titles

    :param title: str - title to add
    :return: if method is successfully return true, else false
    """
    return jsonify(AddressParserService.add_address_title(title))


@app.route('/address-title/<title>', methods=['DELETE'])
def delete_address_title(title: str):
    """
    Interface of deleting an address in address_titles

    :param title: str - title to delete
    :return: if method is successfully return true, else false
    """
    return jsonify(AddressParserService.delete_address_title(title))


@app.route('/address-title', methods=['GET'])
def get_address_titles():
    """
    Get all current address titles of AddressParserService

    :return: list[str] - list containing all titles, which are recognized
    """
    return jsonify(AddressParserService.get_address_titles())


