from src.enums.gender_enum import GenderEnum
from src.enums.language_enum import LanguageEnum


class Address:
    def __init__(self, title: list[str], sur_name: str, name: str, gender: GenderEnum, address: str):
        self.title = title
        self.sur_name = sur_name
        self.name = name
        self.gender = gender
        self.address = address

    def get_letter_address(self, language: LanguageEnum) -> str:
        """
        Method to get formatted letter address string of this Address object

        :param language: LanguageEnum - language of the formatted letter address string
        :return: str - formatted letter address in specified language
        """
        letter_address = " ".join(f"{self.address} {' '.join(self.title)} {self.name} {self.sur_name}".split())
        if language is LanguageEnum.german:
            if self.gender is GenderEnum.not_specified:
                return "Sehr geehrte Damen und Herren"
            elif self.gender is GenderEnum.non_binary:
                return f"Guten Tag {letter_address}"
            elif self.gender is GenderEnum.male:
                return f"Sehr geehrter {letter_address}"
            elif self.gender is GenderEnum.female:
                return f"Sehr geehrte {letter_address}"
        elif language is LanguageEnum.english:
            return f"Dear {letter_address}"

    def serialize(self):
        """
        Method to convert Address object to dictionary for json conversion

        :return: dictionary of Address object
        """
        return {
            "address_titles": self.title,
            "sur_name": self.sur_name,
            "name": self.name,
            "gender": self.gender.value,
            "address": self.address
        }

    @staticmethod
    def deserialize(data: dict):
        """
        Method to convert json/dictionary-object to Address object

        :param data: dict - dictionary to convert
        :return: Address - converted Address object from input data
        """
        return Address(title=data["address_titles"], sur_name=data["sur_name"], name=data["name"],
                       gender=GenderEnum(data["gender"]), address=data["address"])
